
public class Rolo {
	private static String[] valores = {"Maçã", "Banana", "Sete"};
	private static int valorEspecial = 2;
	
	public static String[] getValores() {
		return valores;
	}
	
	public static int getValorEspecial(){
		return valorEspecial;
	}
}
