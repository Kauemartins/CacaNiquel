
public class Pontuador {
	private int[] sorteios = new int[3];
	private int[] contagem = new int[3];
	
	public Resultado calcular() {
		Resultado resultado = new Resultado();
		String[] valores = Rolo.getValores();
		
//		Fazer sorteios
		for(int i = 0; i < sorteios.length; i++) {
			sorteios[i] = sortear(valores.length);
		}
		
//		Inicializar vetor contagem
		for(int i = 0; i < contagem.length; i++) {
			contagem[i] = 0;
		}
		
//		Contar valores sorteados
		for(int i = 0; i < sorteios.length; i++) {
			int valorSorteado = sorteios[i];
			contagem[valorSorteado]++;
		}
		
		resultado.pontos = calcularPontuacao();
		resultado.valores = new String[sorteios.length];
		
//		Preencher resultados
		for(int i = 0; i < sorteios.length; i++) {
			int posicaoSorteada = sorteios[i];
			String valor = valores[posicaoSorteada];
			resultado.valores[i] = valor;
			
//			resultado.valores[i] = Rolo.getValores()[sorteios[i]];
		}
		
		return resultado;
	}
	
	private int calcularPontuacao() {
		for(int i = 0; i < contagem.length; i++) {
			if(contagem[i] >= 3) {
				if(i == Rolo.getValorEspecial()) {
					return 5000;
				}
				return 1000;
			}
			
			if(contagem[i] == 2) {
				return 100;
			}
		}
		
		return 0;
	}
	
	private int sortear(int maximo) {
		return (int) Math.floor(Math.random() * maximo);
	}
}
